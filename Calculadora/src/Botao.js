import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';

export class Botao extends Component {

    constructor(props){
        super(props);
        this.state = {c:1};
        if(this.props.c) {
            this.state.c = parseInt(this.props.c);
        }
    }

    render(){

        return(

            <TouchableOpacity style={styles.area} onPress={()=>{}}>
                <Text style={styles.text}>{this.props.texto}</Text>
            </TouchableOpacity>
            

        );

    }

}

const styles = StyleSheet.create({

    area:{
        flex:this.state.c,
        justifyContent:'center',
        alignItems:'center',
        borderWidth:0.3,
        borderRadius:5,
        borderColor:'#999',
        backgroundColor:'#E0E0E0'
    },
    text:{
        fontSize:18
    }
});