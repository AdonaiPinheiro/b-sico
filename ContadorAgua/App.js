import React, {Component} from 'react';
import { View, StyleSheet, Text, StatusBar, ImageBackground, TouchableOpacity } from 'react-native';

export default class ContadorAgua extends Component {

  constructor(props){
    super(props);

    this.state = {
      meta:'', 
      consumido:0, 
      status:'Ruim', 
      pct:0, 
      erro:''};
      
    this.beberCopo = this.beberCopo.bind(this);
    this.atualizar = this.atualizar.bind(this);
  }

  atualizar(){
    let s = this.state;
    s.pct = Math.floor(((s.consumido/2000) * 100));
    if(s.pct >= 100){
      s.status = "Bom";
    } else if (s.pct >= 50) {
      s.status = "Tá quase!";
    } else {
      s.status = "Ruim";
    }
    this.setState(s);
  }

  beberCopo(){
    let s = this.state;

    if(s.consumido == 2000) {
      s.erro = "Você já atingiu a meta!"
    } else {
      s.consumido += 200;
    }
    
    this.atualizar();
    this.setState(s);
  }

  render(){

    return(

      <View style={styles.container}>
        <StatusBar hidden={true} />
        <ImageBackground source={require('./images/waterbg.png')}
          style={styles.bgimage}>

          <View style={{flexDirection:'column'}}>
            <View style={styles.topo}>
              <View style={styles.area}>
                <Text style={styles.areaTitulo}>Meta</Text>
                <Text style={styles.areaDados}>2000ml</Text>
              </View>
              <View style={styles.area}>
                <Text style={styles.areaTitulo}>Consumido</Text>
                <Text style={styles.areaDados}>{this.state.consumido}ml</Text>
              </View>
              <View style={styles.area}>
                <Text style={styles.areaTitulo}>Status</Text>
                <Text style={styles.areaDados}>{this.state.status}</Text>
              </View>
            </View>

            <View style={styles.pctArea}>
              <Text style={styles.pctText}>{this.state.pct}%</Text>
              <Text style={styles.meta}>{this.state.erro}</Text>
            </View>

            <View style={styles.btnArea}>
              <TouchableOpacity style={styles.btn} onPress={this.beberCopo}>
                <Text style={styles.btnTexto}>Beber 200ml</Text>
              </TouchableOpacity>
            </View>

          </View>

        </ImageBackground>
      </View>

    );

  }

}

const styles = StyleSheet.create({
  container:{
    flex:1
  },
  bgimage:{
    flex:1,
    width:null
  },
  topo:{
    flex:1,
    flexDirection:'row',
    marginTop:70
  },
  area:{
    flex:1,
    alignItems:'center'
  },
  areaTitulo:{
    color:'#45B2FC',
    fontSize:15
  },
  areaDados:{
    color:'#2B4274',
    fontSize:18,
    fontWeight:'bold'
  },
  pctArea:{
    marginTop:275,
    alignItems:'center'
  },
  pctText:{
    fontSize:70,
    color:'#fff',
    backgroundColor:'transparent',
  },
  btnArea:{
    marginTop:125,
    alignItems:'center'
  },
  btn:{
    borderWidth:0.3,
    backgroundColor:'#FFF',
    paddingTop:10,
    paddingBottom:10,
    paddingLeft:7,
    paddingRight:7,
    borderRadius:5,
    borderColor:'#FFF'
  },
  btnTexto:{
    color:'#45B2FC',
    fontSize:20
  },
  meta:{
    fontSize:20,
    fontWeight:'bold',
    color:'#160C84'
  }
});