import React, { Component } from 'react';
import { View, StyleSheet, Image, Text, TouchableOpacity } from 'react-native';

export default class Cronometro extends Component {

  constructor(props) {
    super(props);
    this.state = {timer:0.0, btn:'Vai!'};
    this.time = null;

    this.vai = this.vai.bind(this);
    this.limpar = this.limpar.bind(this);
  }

  vai(){
    if (this.time != null) {
      //Parar Timer
      let s = this.state;
      clearInterval(this.time);
      this.time = null;
      s.btn = "Vai!";
      this.setState(s);
    } else {
      //Começar Timer
      this.time = setInterval(()=>{
        let s = this.state;
        s.timer += 0.1;
        s.btn = "Parar";
        this.setState(s);
      }, 100);
    }
  }

  limpar(){
    if (this.time != null) {
      //Parar Timer
      clearInterval(this.time);
      this.time = null;
    }

    let s = this.state;
    s.timer = 0;
    s.btn = "Vai!"
    this.setState(s);
  }

  render () {
    return(

      <View style={styles.container}>
        <Image source={require('./images/relogio.png')} />
        <Text style={styles.timer}>{this.state.timer.toFixed(1)}</Text>

        <View style={styles.areaBtn}>
          <TouchableOpacity style={styles.botao} onPress={this.vai}>
            <Text style={styles.txtBtn}>{this.state.btn}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.botao} onPress={this.limpar}>
            <Text style={styles.txtBtn}>Resetar</Text>
          </TouchableOpacity>
        </View>

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    flexDirection:'column',
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:'#2C1F30'
  },
  timer:{
    color:'#DAA07A',
    fontSize:80,
    fontWeight:'bold',
    backgroundColor:'transparent',
    marginTop:-160
  },
  areaBtn:{
    flexDirection:'row',
    height:40,
    marginTop:80
  },
  botao:{
    flex:1,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:'#886532',
    height:40,
    borderRadius:5,
    margin:10
  },
  txtBtn:{
    fontSize:18,
    color:'#FFF'
  }
});